from openerp.osv import orm, fields
from openerp import SUPERUSER_ID
from email.utils import formataddr

class mail_message(orm.Model):
    _name = 'mail.message'
    _inherit = 'mail.message'

    def _get_reply_to(self, cr, uid, values, context={}):
        """ Return a specific reply_to: alias of the document through message_get_reply_to
            or take the email_from
        """
        # my hack
        this = self.pool.get('res.users').browse(cr, SUPERUSER_ID, uid, context=context)
        if this.alias_name and this.alias_domain:
            return formataddr((this.name, '%s@%s' % (this.alias_name, this.alias_domain)))
        # Pass futher
        model, res_id, email_from = values.get('model'), values.get('res_id'), values.get('email_from')
        ctx = dict(context, thread_model=model)
        return self.pool['mail.thread'].message_get_reply_to(cr, uid, [res_id], default=email_from, context=ctx)[res_id]