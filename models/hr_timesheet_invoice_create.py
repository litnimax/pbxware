from openerp.osv import fields, osv

class hr_timesheet_invoice_create(osv.osv_memory):
    _name = 'hr.timesheet.invoice.create'
    _inherit = 'hr.timesheet.invoice.create'

    _defaults = {
	'date':  1,
        'name':  1,
	'time': 1,
    }

