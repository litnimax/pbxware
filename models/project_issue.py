from openerp.osv import orm, fields
from email.utils import parseaddr

class project_issue(orm.Model):
    _name = 'project.issue'
    _inherit = 'project.issue'

    def route_issue(self, cr, uid, ids, context=None):
        """
        1. Find open project. If found, take project manager and assign issue owner.
        2. If no project find contact's salesfman and assign issue owner
        """
        issue = self.pool['project.issue'].browse(cr, uid, ids, context)[0]
        issue_email = parseaddr(issue.email_from)[1]
        project_id = self.pool.get('project.project').search(cr, uid, [
                ('partner_id.email','=', '%s' % issue_email),
                ('state', '=', 'open')
            ],
            context=context)
        if project_id:
            project = self.pool.get('project.project').browse(cr, uid, project_id, context=context)
            issue.write({'project_id': project.id, 'user_id': project.user_id.id})
        else:
            salesperson_id = issue.partner_id.user_id
            if salesperson_id:
                issue.user_id = salesperson_id
            else:
                # nobody found, notify all
                group_id = self.pool.get('ir.model.data').get_object_reference(cr, uid, 'project', 'group_project_user')[1]
                group = self.pool.get('res.groups').browse(cr, uid, [group_id], context=context)
                followers = []
                [followers.append(user.partner_id.id) for user in group.users]
                issue.message_follower_ids = followers
                issue.message_post(context=context, partner_ids=followers, body='Issue created from uknown customer. Please check it.')




    def _read_group_stage_ids(self, cr, uid, ids, domain, read_group_order=None, access_rights_uid=None, context=None):
        access_rights_uid = access_rights_uid or uid
        stage_obj = self.pool.get('project.task.type')
        order = stage_obj._order
        # lame hack to allow reverting search, should just work in the trivial case
        if read_group_order == 'stage_id desc':
            order = "%s desc" % order
        # retrieve section_id from the context and write the domain
        # - ('id', 'in', 'ids'): add columns that should be present
        # - OR ('case_default', '=', True), ('fold', '=', False): add default columns that are not folded
        # - OR ('project_ids', 'in', project_id), ('fold', '=', False) if project_id: add project columns that are not folded
        search_domain = []
        project_id = self._resolve_project_id_from_context(cr, uid, context=context)
        if project_id:
            #search_domain += ['|', ('project_ids', '=', project_id)]
	    search_domain += [('project_ids', '=', project_id)]
        # My ONLY HACK TO SHOW ALL STAGES
	#search_domain += [('id', 'in', ids)]
        # perform search
        stage_ids = stage_obj._search(cr, uid, search_domain, order=order, access_rights_uid=access_rights_uid, context=context)
        result = stage_obj.name_get(cr, access_rights_uid, stage_ids, context=context)
        # restore order of the search
        result.sort(lambda x,y: cmp(stage_ids.index(x[0]), stage_ids.index(y[0])))

        fold = {}
        for stage in stage_obj.browse(cr, access_rights_uid, stage_ids, context=context):
            fold[stage.id] = stage.fold or False
        return result, fold
