# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
{
    'name' : 'PBXWare tuning',
    'version' : '1.0',
    'author' : 'Litnimax',
    'category' : 'Hidden',
    'description' : """
PBXWare Projects Tuning
=========================

Tuning covers:
--------------------------------------------
    * Roles
    * Permissions
    * Custom views
    """,
    'website': 'http://www.pbxware.ru',
    'images' : [],
    'depends' : [
	'account',
	'account_analytic_analysis',
	'account_voucher',
	'base',
	'base_action_rule',
	'base_setup',
	'crm',
	'hr_timesheet_invoice',
	'im_chat',
	'mail', 
	'note',
	'portal',
	'portal_project',
	'portal_project_issue',
	'project',
	'project_timesheet',
	'project_issue',
	'project_issue_sheet',
	'sale',
	'web_adblock',
    ],
    'data': [
        'project.xml',
        'partner.xml',
        'portal.xml',
	'sale.xml',
	'hr.xml',
	'account.xml',
	'account_voucher.xml',
        'project_issue.xml',
        'security/ir.model.access.csv',
        'resource_demo.xml',
    ],
    'qweb' : [],
    'installable': True,
    'auto_install': False,
}

